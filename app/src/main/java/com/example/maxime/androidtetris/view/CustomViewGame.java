package com.example.maxime.androidtetris.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import com.example.maxime.androidtetris.R;
import com.example.maxime.androidtetris.activities.Game;
import com.example.maxime.androidtetris.item.Model;
import com.example.maxime.androidtetris.item.ModelFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rweisha-PC on 27/02/2017.
 */



public class CustomViewGame extends View{

    // Pinceau :
    private Paint paint_box,paint_triangle,paint_box_up;

    private int cote;

    //ColorVariable
    private int form1FirstColor = R.color.firstColorOfBricks_first;
    private int form1SecondColor = R.color.secondColorOfBricks_first;
    private int form1ThirdColor = R.color.thirdColorOfBricks_first;

    private int form2FirstColor = R.color.firstColorOfBricks_second;
    private int form2SecondColor = R.color.secondColorOfBricks_second;
    private int form2ThirdColor = R.color.thirdColorOfBricks_second;

    private int form3FirstColor = R.color.firstColorOfBricks_third;
    private int form3SecondColor = R.color.secondColorOfBricks_third;
    private int form3ThirdColor = R.color.thirdColorOfBricks_third;

    private int form4FirstColor = R.color.firstColorOfBricks_fourth;
    private int form4SecondColor = R.color.secondColorOfBricks_fourth;
    private int form4ThirdColor = R.color.thirdColorOfBricks_fourth;

    private int form5FirstColor = R.color.firstColorOfBricks_fifth;
    private int form5SecondColor = R.color.secondColorOfBricks_fifth;
    private int form5ThirdColor = R.color.thirdColorOfBricks_fifth;

    private int form6FirstColor = R.color.firstColorOfBricks_sixth;
    private int form6SecondColor = R.color.secondColorOfBricks_sixth;
    private int form6ThirdColor = R.color.thirdColorOfBricks_sixth;

    private int form7FirstColor = R.color.firstColorOfBricks_seventh;
    private int form7SecondColor = R.color.secondColorOfBricks_seventh;
    private int form7ThirdColor = R.color.thirdColorOfBricks_seventh;


    //private List<Point[]> allForm = new ArrayList<>();

    private int [][] general_collision = new int [21][12];

    private List<Model> general = new ArrayList<>();

    private ModelFactory modelFactory = new ModelFactory();

    public Model getModelCurrent() {
        return modelCurrent;
    }

    private Model modelCurrent = modelFactory.randomModel();

    private int score = 0;
    private int niveau = 1;
    private int thread_sleep = 400;


    public CustomViewGame(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomViewGame(Context context)
    {
        super(context);
        init(context, null); // normalement jamais appelé
    }

    public CustomViewGame(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (context != null && attrs != null) {

        }

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);

        for (int i = 0; i< 21 ; i++){
            for( int j = 0; j< 12; j++){
                if(j==0 || j==11 || i == 20){
                    general_collision[i][j] = 1000;
                }else{

                    general_collision[i][j] = 0;
                }
            }
        }

    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        cote = Math.min(this.getMeasuredWidth()/10,this.getMeasuredHeight()/20);

        for (int i = 0; i< general.size() ; i++){
            for(int j = 0; j<=3; j++){

                switch (general.get(i).getColor()){
                    case 1:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(), form1FirstColor, form1SecondColor, form1ThirdColor);
                        break;
                    case 2:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(),form2FirstColor,form2SecondColor,form2ThirdColor);
                        break;
                    case 3:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(),form3FirstColor,form3SecondColor,form3ThirdColor);
                        break;
                    case 4:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(),form4FirstColor,form4SecondColor,form4ThirdColor);
                        break;
                    case 5:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(),form5FirstColor,form5SecondColor,form5ThirdColor);
                        break;
                    case 6:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(),form6FirstColor,form6SecondColor,form6ThirdColor);
                        break;
                    case 7:
                        DessinBricks(canvas, general.get(i).getPoint(j).getX(), general.get(i).getPoint(j).getY(),form7FirstColor,form7SecondColor,form7ThirdColor);
                        break;
                }
            }
        }


        for (int i =0;i<=3;i++){

            switch (modelCurrent.getColor()){
                case 1:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(), form1FirstColor, form1SecondColor, form1ThirdColor);
                    break;
                case 2:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(),form2FirstColor,form2SecondColor,form2ThirdColor);
                    break;
                case 3:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(),form3FirstColor,form3SecondColor,form3ThirdColor);
                    break;
                case 4:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(),form4FirstColor,form4SecondColor,form4ThirdColor);
                    break;
                case 5:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(),form5FirstColor,form5SecondColor,form5ThirdColor);
                    break;
                case 6:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(),form6FirstColor,form6SecondColor,form6ThirdColor);
                    break;
                case 7:
                    DessinBricks(canvas, modelCurrent.getPoint(i).getX(), modelCurrent.getPoint(i).getY(),form7FirstColor,form7SecondColor,form7ThirdColor);
                    break;
            }
        }

    }

    protected void DessinBricks(Canvas canvas , float x, float y, int firstColor, int secondColor, int thirdColor)
    {
        float pourcentage = (float) (cote*10/100);

        x *= cote;
        x -= cote;
        y *= cote;
        y -= cote;

        float bottom = y + cote-1;
        float right = x + cote-1;


        paint_box = new Paint();
        paint_box.setColor(getResources().getColor(firstColor));

        paint_triangle = new Paint();
        paint_triangle.setColor(getResources().getColor(secondColor));

        paint_box_up = new Paint();
        paint_box_up.setColor(getResources().getColor(thirdColor));

        canvas.drawRect(x,y,right,bottom,paint_box);
        drawTriangle(canvas,paint_triangle,x,y);
        canvas.drawRect(x+pourcentage,y+pourcentage,right-pourcentage,bottom-pourcentage,paint_box_up);

    }


    public void drawTriangle(Canvas canvas, Paint paint, float x, float y) {

        float pourcentageFromUp = (float) (cote*3.33/100);

        Path path = new Path();
        path.moveTo(x+pourcentageFromUp,y+pourcentageFromUp); // top left
        path.lineTo(x+ cote-pourcentageFromUp, y+pourcentageFromUp); // top right
        path.lineTo(x+pourcentageFromUp, y+cote-pourcentageFromUp); // bottom left
        path.lineTo(x+pourcentageFromUp, y+pourcentageFromUp); // top left
        path.close();

        canvas.drawPath(path, paint);

    }

    public void downWithCollision(){

        boolean down = true;
        for (int i = 0; i <= 3; i++) {
            if(general_collision[modelCurrent.getPoint(i).getY()+1][modelCurrent.getPoint(i).getX()] != 0){
                down = false;
                loadGeneralArrayWithCurrentForm(modelCurrent);
                invalidate();
            }
        }

        if(down) {
            modelCurrent.descendre();
        }

    }

    public void swipeRightWithColision(){
        boolean right = true;
        for (int i = 0; i <= 3; i++) {
            if(general_collision[modelCurrent.getPoint(i).getY()][modelCurrent.getPoint(i).getX()+1] != 0){
                right = false;
            }
        }

        if(right) {
            modelCurrent.swipeRight();
        }
    }

    public void swipeLeftWithColision(){
        boolean right = true;
        for (int i = 0; i <= 3; i++) {
            if(general_collision[modelCurrent.getPoint(i).getY()][modelCurrent.getPoint(i).getX()-1] != 0){
                right = false;
            }
        }

        if(right) {
            modelCurrent.swipeLeft();
        }
    }

    public void interpretDownWithCollision(){
        modelCurrent.swipeDownWithColision(general_collision);
    }



    public void loadGeneralArrayWithCurrentForm(Model modelcurrent){


        for(int i = 0; i<=3 ; i++){
            general_collision[modelcurrent.getPoint(i).getY()][modelcurrent.getPoint(i).getX()] = 1000;
        }

        general.add(0,new Model(modelcurrent.getPoint(0),modelcurrent.getPoint(1),modelcurrent.getPoint(2),modelcurrent.getPoint(3),modelcurrent.getColor()));

        checkIfOnLineComplete();

        modelCurrent = modelFactory.randomModel();
        for(int i = 0; i<=3 ; i++){
            if(general_collision[modelCurrent.getPoint(i).getY()][modelCurrent.getPoint(i).getX()] != 0){
                resetGame();
            }
        }

    }

    public void resetGame(){
        score = 0;
        niveau = 1;
        thread_sleep = 2000;
        general = new ArrayList<>();
        for (int i = 0; i< 21 ; i++){
            for( int j = 0; j< 12; j++){
                if(j==0 || j==11 || i == 20){
                    general_collision[i][j] = 1000;
                }else{

                    general_collision[i][j] = 0;
                }
            }
        }
        modelCurrent = modelFactory.randomModel();
    }

    public void checkIfOnLineComplete(){
        boolean lineComplete = false;
        ArrayList<Integer> lines = new ArrayList<>();
        for (int i = 0; i< 20 ; i++){
            int nbr_column = 0;
            for( int j = 1; j< 11; j++){
                if(general_collision[i][j] == 1000){
                    nbr_column++;
                }
                if(nbr_column == 10){
                    lines.add(0,i);
                    lineComplete = true;
                }
            }
        }

        if(lineComplete){


            for(int i = lines.size(); i>0 ; i--){
                updateScoreAndNniveau(50);
                for( int d = 1; d < 11; d++){
                    general_collision[lines.get(i-1)][d] = 0;
                }

                for (int y = lines.get(i-1); y > 0 ; y--){
                    for( int x = 1; x < 11; x++){
                        if(general_collision[y][x] == 1000){
                            general_collision[y][x] = 0;
                            general_collision[y+1][x] = 1000;
                        }
                    }
                }

                for (int h = 0; h < general.size(); h++){
                    for(int x = 0; x <= 3; x++){
                        if(general.get(h).getPoint(x).getY() == lines.get(i-1)){
                            general.get(h).deletePoint(x);
                        }
                    }
                }
                for(int m = 0; m < general.size(); m++){
                    for(int x = 0; x <= 3; x++){
                        if(general.get(m).getPoint(x).getY() < lines.get(i-1)){
                            general.get(m).getPoint(x).setPoint(general.get(m).getPoint(x).getX(),general.get(m).getPoint(x).getY()+1);
                        }
                    }
                }

            }
        }

    }

    public void updateScoreAndNniveau(int score){
        int tmp_score = this.score;
        this.setScore(tmp_score+=score);
        if(tmp_score%100 == 0){
            if(thread_sleep > 200){
                thread_sleep-=200;
            }
            int tmp_niveau = this.getNiveau();
            this.setNiveau(tmp_niveau+=1);
        }
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getThread_sleep() {
        return thread_sleep;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public int getScore() {
        return score;
    }

    public int getNiveau() {
        return niveau;
    }
}
