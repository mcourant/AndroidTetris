package com.example.maxime.androidtetris.item;

/**
 * Created by Rweisha-PC on 20/04/2017.
 */

public class Point {

    private int x;
    private int y;
    private int color;

    public Point(){}


    public Point(int x, int y){
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public void setPoint(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Point getPoint(){
        return this;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public int getColor() {
        return color;
    }


}