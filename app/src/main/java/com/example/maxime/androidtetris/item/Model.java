package com.example.maxime.androidtetris.item;

import android.util.Log;
import android.widget.Toast;

/**
 * Created by Rweisha-PC on 20/04/2017.
 */
public class Model{

    Point p1 =new Point();
    Point p2 =new Point();
    Point p3 =new Point();
    Point p4 =new Point();
    int color;

    public Model(){

    }



    public Model(Point p1, Point p2, Point p3, Point p4, int color){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.color = color;
    }

    public Point getPoint(int i){
        switch (i){
            case 0:
                return this.p1;
            case 1:
                return this.p2;
            case 2:
                return this.p3;
            case 3:
                return this.p4;
            default:
                return this.p1;
        }
    }

    public void setPoint(int i, Point p){
        switch (i){
            case 0:
                this.p1.setPoint(p.getX(),p.getY());
            case 1:
                this.p2.setPoint(p.getX(),p.getY());
            case 2:
                this.p3.setPoint(p.getX(),p.getY());
            case 3:
                this.p4.setPoint(p.getX(),p.getY());
            default:
                this.p1.setPoint(p.getX(),p.getY());
        }
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void descendre(){
        this.p1.setPoint(this.p1.getX(),this.p1.getY()+1);
        this.p2.setPoint(this.p2.getX(),this.p2.getY()+1);
        this.p3.setPoint(this.p3.getX(),this.p3.getY()+1);
        this.p4.setPoint(this.p4.getX(),this.p4.getY()+1);
    }

    public void monter(){
        this.p1.setPoint(this.p1.getX(),this.p1.getY()-1);
        this.p2.setPoint(this.p2.getX(),this.p2.getY()-1);
        this.p3.setPoint(this.p3.getX(),this.p3.getY()-1);
        this.p4.setPoint(this.p4.getX(),this.p4.getY()-1);
    }

    public void rotation(){

        Point origine = new Point(this.p3.getX(),this.p3.getY());
        boolean turn = true;

        // changement d'origne
        for (int i = 0; i <= 3; i++) {

            this.getPoint(i).setPoint(this.getPoint(i).getX()-origine.getX(), this.getPoint(i).getY() -origine.getY() );

        }
/*
        for (int i = 0; i<=3;i++){
            System.out.println(general_collision[this.getPoint(i).getX()+origine.getY()][(-this.getPoint(i).getY())+origine.getX()]);
            try{
                if(turn && general_collision[this.getPoint(i).getX()+origine.getY()][(-this.getPoint(i).getY())+origine.getX()] != 0){
                    turn = false;
                }
            }catch (ArrayIndexOutOfBoundsException e){
                Log.e("TEst","Test");
            }
        }
*/
        //if(turn) {
        // rotation par rapport à l'origine
        // idée x =- y
        // y =x
        for (int i = 0; i <= 3; i++) {
            this.getPoint(i).setPoint(-this.getPoint(i).getY(),this.getPoint(i).getX() );
        }
        //}

        // retour repère absolu
        for (int i = 0; i <= 3; i++) {
            this.getPoint(i).setPoint( this.getPoint(i).getX() + origine.getX(), this.getPoint(i).getY() + origine.getY());
        }

    }

    public void swipeRight(){

        this.p1.setPoint(this.p1.getX()+1,this.p1.getY());
        this.p2.setPoint(this.p2.getX()+1,this.p2.getY());
        this.p3.setPoint(this.p3.getX()+1,this.p3.getY());
        this.p4.setPoint(this.p4.getX()+1,this.p4.getY());

    }

    public void swipeLeft(){

        this.p1.setPoint(this.p1.getX()-1,this.p1.getY());
        this.p2.setPoint(this.p2.getX()-1,this.p2.getY());
        this.p3.setPoint(this.p3.getX()-1,this.p3.getY());
        this.p4.setPoint(this.p4.getX()-1,this.p4.getY());

    }

    public void swipeDownWithColision(int [][] general_collision){

        boolean collision = false;
        while(!collision){

            descendre();

            if(general_collision[this.p1.getY()][this.p1.getX()] != 0 || general_collision[this.p2.getY()][this.p2.getX()] != 0 || general_collision[this.p3.getY()][this.p3.getX()] != 0 || general_collision[this.p4.getY()][this.p4.getX()] != 0){
                monter();
                collision = true;
            }

        }

    }

    public void deletePoint(int i){
        switch (i){
            case 0:
                this.p1 = new Point();
                break;
            case 1:
                this.p2 = new Point();
                break;
            case 2:
                this.p3 = new Point();
                break;
            case 3:
                this.p4 = new Point();
                break;
        }
    }

}