package com.example.maxime.androidtetris;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;

import com.example.maxime.androidtetris.activities.Game;


public class MainActivity extends AppCompatActivity {

    ToggleButton easyButton;
    ToggleButton moyenButton;
    ToggleButton hardButton;

    SharedPreferences.Editor editor;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        easyButton = (ToggleButton) findViewById(R.id.easyButton);
        moyenButton = (ToggleButton) findViewById(R.id.middleButton);
        hardButton= (ToggleButton) findViewById(R.id.hardButton);

        preferences = getSharedPreferences("difficulties",getApplication().MODE_PRIVATE);
        editor = preferences.edit();


        int difficulty = preferences.getInt("dif",0);
        switch (difficulty){
            case 0:
                easyButton.setChecked(true);
                moyenButton.setChecked(false);
                hardButton.setChecked(false);
                break;
            case 1:
                easyButton.setChecked(false);
                moyenButton.setChecked(true);
                hardButton.setChecked(false);
                break;
            case 2:
                easyButton.setChecked(false);
                moyenButton.setChecked(false);
                hardButton.setChecked(true);
                break;
            default:
                easyButton.setChecked(false);
                moyenButton.setChecked(true);
                hardButton.setChecked(false);
                break;
        }

        easyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("dif",0);
                editor.apply();
                moyenButton.setChecked(false);
                hardButton.setChecked(false);
            }
        });

        moyenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("dif",1);
                editor.apply();
                easyButton.setChecked(false);
                hardButton.setChecked(false);
                Intent intent = new Intent(MainActivity.this, Game.class);
                startActivity(intent);
            }
        });

        hardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("dif",2);
                editor.apply();
                easyButton.setChecked(false);
                moyenButton.setChecked(false);
            }
        });

    }
}
