package com.example.maxime.androidtetris.activities;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maxime.androidtetris.MainActivity;
import com.example.maxime.androidtetris.R;
import com.example.maxime.androidtetris.item.OnSwipeTouchListener;
import com.example.maxime.androidtetris.view.CustomViewGame;

import java.util.Timer;
import java.util.TimerTask;

public class Game extends AppCompatActivity  {

    private ImageView parameters;
    private CustomViewGame view;
    private ImageView reset;
    private ImageView home;

    private TextView score_Tv;
    private TextView niveau_Tv;

    CustomViewGame jeuxForAll;
    private int thread_sleep = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_le_jeux);

        parameters = (ImageView) findViewById(R.id.menu_parametre);
        jeuxForAll = (CustomViewGame) findViewById(R.id.customViewPlay);
        reset = (ImageView) findViewById(R.id.reloadgame);

        score_Tv = (TextView) findViewById(R.id.scoreTv);
        niveau_Tv = (TextView) findViewById(R.id.niveauTv);



        new Thread(new Runnable()
        {

            @Override
            public void run()
            {
                while (!Thread.interrupted())
                    try
                    {

                        System.out.println("threadSleep : "+thread_sleep);
                        System.out.println("_______________");
                        Thread.sleep(thread_sleep);
                        runOnUiThread(new Runnable() // start actions in UI thread
                        {

                            @Override
                            public void run()
                            {
                                CustomViewGame jeux = (CustomViewGame) findViewById(R.id.customViewPlay);
                                jeux.downWithCollision();
                                jeux.invalidate();

                                score_Tv.setText(""+jeuxForAll.getScore());
                                    niveau_Tv.setText(""+jeuxForAll.getNiveau());
                                thread_sleep = jeuxForAll.getThread_sleep();

                            }
                        });
                    }
                    catch (InterruptedException e)
                    {
                        //
                    }
            }
        }).start();


        jeuxForAll.setOnTouchListener(new OnSwipeTouchListener(this) {
                    public void onSwipeTop() {
                    }
                    public void onSwipeRight() {
                        jeuxForAll.swipeRightWithColision();
                        jeuxForAll.invalidate();
                    }
                    public void onSwipeLeft() {
                        jeuxForAll.swipeLeftWithColision();
                        jeuxForAll.invalidate();
                    }
                    public void onSwipeBottom() {
                        jeuxForAll.interpretDownWithCollision();
                        jeuxForAll.invalidate();
                    }
                    public void onTouch(){
                        jeuxForAll.getModelCurrent().rotation();
                        jeuxForAll.invalidate();
                    }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jeuxForAll.resetGame();
            }
        });
        parameters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }


}
