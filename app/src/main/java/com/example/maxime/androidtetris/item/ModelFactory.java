package com.example.maxime.androidtetris.item;

import android.graphics.PorterDuff;

/**
 * Created by Rweisha-PC on 20/04/2017.
 */

public class ModelFactory {

    public Model randomModel(){
        int random = (int) (Math.random()*6);

        switch (random){
            case 0:
                return createModel1();
            case 1:
                return createModel2();
            case 2:
                return createModel3();
            case 3:
                return createModel4();
            case 4:
                return createModel5();
            case 5:
                return createModel6();
            case 6:
                return createModel7();
            default:
                return createModel1();

        }
    }

    protected Model createModel1(){
        return new Model(new Point(5,2), new Point(5,3), new Point(5,4), new Point(5,5),7);
    }

    protected Model createModel2(){
        return new Model(new Point(4,2), new Point(5,2), new Point(5,3), new Point(6,3),6);
    }

    protected Model createModel3(){
        return new Model(new Point(4,2), new Point(5,2), new Point(5,3), new Point(5,4),5);
    }

    protected Model createModel4(){
        return new Model(new Point(5,2), new Point(6,2), new Point(5,3), new Point(5,4),4);
    }

    protected Model createModel5(){
        return new Model(new Point(4,2), new Point(5,2), new Point(5,3), new Point(4,3),3);
    }

    protected Model createModel6(){
        return new Model(new Point(5,2), new Point(6,3), new Point(6,2), new Point(7,2),2);
    }

    protected Model createModel7(){
        return new Model(new Point(5,3), new Point(6,3), new Point(6,2), new Point(7,2),1);
    }

}



